import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SeeallcoursenewPage } from './seeallcoursenew.page';

const routes: Routes = [
  {
    path: '',
    component: SeeallcoursenewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SeeallcoursenewPageRoutingModule {}
