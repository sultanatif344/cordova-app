import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SeeallcoursenewPageRoutingModule } from './seeallcoursenew-routing.module';

import { SeeallcoursenewPage } from './seeallcoursenew.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SeeallcoursenewPageRoutingModule
  ],
  declarations: [SeeallcoursenewPage]
})
export class SeeallcoursenewPageModule {}
