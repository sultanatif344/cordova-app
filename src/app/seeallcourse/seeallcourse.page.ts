import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MenuController, NavController,ViewWillEnter } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ProgressBarComponent } from '../progress-bar/progress-bar.component';

@Component({
  selector: 'app-seeallcourse',
  templateUrl: './seeallcourse.page.html',
  styleUrls: ['./seeallcourse.page.scss'],
})
export class SeeallcoursePage implements ViewWillEnter {
  connectionslist: any;
  searchvalue="";
  zitappurl: string;
  totalcards: any;
  Fullname: any;
  connectionslist1: any;
  constructor(public menuCtrl: MenuController,public navCtrl: NavController, private http: HttpClient,private storage: Storage,private route: Router){ }

  ionViewWillEnter() {

    this.menuCtrl.close();

        
    this.storage.get('loginuid').then((val) => {
  
      this.dotasksall(val);
      });

      
    
  }



  dotasksall(loginuid)
  {

    
    var url = 'https://arttcssacademy.com/app/services/getallcourses.php?UID='+loginuid;
    console.log(url);
     this.http.get(url).subscribe(data=>{
       console.log(data);
       this.connectionslist = data;
       
       console.log(this.connectionslist);
       },err=>{console.log(err);});


    var url = 'https://arttcssacademy.com/app/services/getuserdetails.php?ID='+loginuid;
    console.log(url);
    this.http.get(url).subscribe(data=>{
      // console.log(data['FirstName']);
      this.Fullname = data['Fullname'];
     
		  },err=>{console.log(err);});



       
    var url = 'https://arttcssacademy.com/app/services/getuserscourse.php?UID='+loginuid;
    console.log(url);
     this.http.get(url).subscribe(data=>{
       console.log(data);
       this.connectionslist1 = data;
       
       console.log(this.connectionslist1);
       },err=>{console.log(err);});

        
  }
  
}
