import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SeeallcoursePageRoutingModule } from './seeallcourse-routing.module';

import { SeeallcoursePage } from './seeallcourse.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SeeallcoursePageRoutingModule
  ],
  declarations: [SeeallcoursePage]
})
export class SeeallcoursePageModule {}
