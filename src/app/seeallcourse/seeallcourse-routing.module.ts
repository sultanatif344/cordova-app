import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SeeallcoursePage } from './seeallcourse.page';

const routes: Routes = [
  {
    path: '',
    component: SeeallcoursePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SeeallcoursePageRoutingModule {}
