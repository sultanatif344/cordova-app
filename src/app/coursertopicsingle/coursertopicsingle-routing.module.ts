import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CoursertopicsinglePage } from './coursertopicsingle.page';

const routes: Routes = [
  {
    path: '',
    component: CoursertopicsinglePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CoursertopicsinglePageRoutingModule {}
