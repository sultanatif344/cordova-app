import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NavController,ViewWillEnter } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-coursertopicsingle',
  templateUrl: './coursertopicsingle.page.html',
  styleUrls: ['./coursertopicsingle.page.scss'],
})
export class CoursertopicsinglePage implements ViewWillEnter {
  tid:string;
  public userdetails: any = [];
  TopicDescription: any;
  TopicName: any;
  CID: any;
  iscompleted: string;
  cid: string;
  constructor(private _Activatedroute:ActivatedRoute,public navCtrl: NavController, private http: HttpClient,private storage: Storage,public route: Router){ 

    this.tid =this._Activatedroute.snapshot.paramMap.get("id");
    this.iscompleted =this._Activatedroute.snapshot.paramMap.get("ic");
    this.cid =this._Activatedroute.snapshot.paramMap.get("cid");
  }

  ionViewWillEnter() {

    var url = 'https://m-prolink.com/Edinfi/services/gettopicdetails.php?ID='+this.tid;
    console.log(url);
    this.http.get(url).subscribe(data=>{
      console.log(data);
      this.CID = data['CID'];
      this.TopicName = data['TopicName'];
      this.TopicDescription = data['TopicDescription'];
      console.log(this.TopicDescription);
      // this.TopicDescription = this.TopicDescription.replace(/(?:\r\n|\r|\n)/g, ' </br> ');
    
     
      
		  },err=>{console.log(err);});

      
 


  }

  makecompleted()
  {
    
    this.storage.get('loginuid').then((val) => {
    var url = 'https://m-prolink.com/Edinfi/services/maketopiccompleted.php?CID='+this.CID+'&TID='+this.tid+'&UID='+val;
    console.log(url);
    this.http.get(url).subscribe(data=>{
      console.log(data);
      
      this.route.navigateByUrl('/courserunning/'+this.CID);
     
      
		  },err=>{console.log(err);});

    });
  }

}
