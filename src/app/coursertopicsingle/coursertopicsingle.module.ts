import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CoursertopicsinglePageRoutingModule } from './coursertopicsingle-routing.module';

import { CoursertopicsinglePage } from './coursertopicsingle.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CoursertopicsinglePageRoutingModule
  ],
  declarations: [CoursertopicsinglePage]
})
export class CoursertopicsinglePageModule {}
