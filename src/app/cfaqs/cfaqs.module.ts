import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CfaqsPageRoutingModule } from './cfaqs-routing.module';

import { CfaqsPage } from './cfaqs.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CfaqsPageRoutingModule
  ],
  declarations: [CfaqsPage]
})
export class CfaqsPageModule {}
