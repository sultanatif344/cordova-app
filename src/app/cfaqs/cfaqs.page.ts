import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NavController,ViewWillEnter } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-cfaqs',
  templateUrl: './cfaqs.page.html',
  styleUrls: ['./cfaqs.page.scss'],
})
export class CfaqsPage implements ViewWillEnter {
  connectionslist: any;
  searchvalue="";
  zitappurl: string;
  totalcards: any;
  tid: string;
  iscompleted: string;
  cid: string;
  constructor(private _Activatedroute:ActivatedRoute,public navCtrl: NavController, private http: HttpClient,private storage: Storage,private route: Router){ 
    
    this.tid =this._Activatedroute.snapshot.paramMap.get("id");
    this.iscompleted =this._Activatedroute.snapshot.paramMap.get("ic");
    this.cid =this._Activatedroute.snapshot.paramMap.get("cid");
  }

  ionViewWillEnter() {

         
    var url = 'https://arttcssacademy.com/app/services/getallcfaqs.php?CID='+this.tid;
    console.log(url);
     this.http.get(url).subscribe(data=>{
       console.log(data);
       this.connectionslist = data;
       this.totalcards = data['length'];
       
       console.log(this.connectionslist);
       },err=>{console.log(err);});
  }

  changetoggle(fid)
  {
   if($("#faq-"+fid).attr("value")=='0')
   {
    $("#faq-"+fid).slideDown();
    $("#faq-"+fid).attr("value","1");
   }
    else
    {
    $("#faq-"+fid).slideUp();
    $("#faq-"+fid).attr("value","0");
    }
  }

}
