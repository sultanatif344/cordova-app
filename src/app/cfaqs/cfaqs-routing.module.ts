import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CfaqsPage } from './cfaqs.page';

const routes: Routes = [
  {
    path: '',
    component: CfaqsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CfaqsPageRoutingModule {}
