import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController, NavController, ViewWillEnter } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { LoaderService } from '../services/loader.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage  implements ViewWillEnter {
  Fullname: any;
  Fathername: any;
  PhoneNumber: any;
  EmailAddress: any;
  StudentID: string;

  constructor(public menuCtrl: MenuController, public navCtrl: NavController, private http: HttpClient,private storage: Storage,private route: Router,private ionLoader: LoaderService){ }


    ionViewWillEnter() {
    
    this.menuCtrl.close();
            
    this.storage.get('loginuid').then((val) => {
  
      this.dotasksall(val);
      });
    
  }
  

  dotasksall(loginuid)
  {
    

    var url = 'https://arttcssacademy.com/app/services/getuserdetails.php?ID='+loginuid;
    console.log(url);
    this.http.get(url).subscribe(data=>{
      // console.log(data['FirstName']);
      this.Fullname = data['Fullname'];
      this.Fathername = data['Fathername'];
      this.EmailAddress = data['EmailAddress'];
      this.PhoneNumber = data['PhoneNumber'];
      this.StudentID = data['StudentID'];
     
		  },err=>{console.log(err);});
  }

 

  updateprofile()
  {


    this.showLoader();
    this.storage.get('loginuid').then((val) => {

    var url = 'https://arttcssacademy.com/app/services/userprofileupdate.php?StudentID='+this.StudentID+'&Fullname='+this.Fullname+'&Fathername='+this.Fathername+'&EmailAddress='+this.EmailAddress+'&PhoneNumber='+this.PhoneNumber+'&UID='+val;
    console.log(url);
    this.http.get(url).subscribe(data=>{
       console.log(data['status']);
    
        // console.log(data['msg']);
        // this.storage.set('loginuid', data[0]['userId']);
     
        
        setTimeout(function() {
         $('.ErroDiv').slideUp("slow");
     }, 5500);
		  },err=>{console.log(err);});

    });


  }

  showLoader() {
    this.ionLoader.showLoader();
    setTimeout(() => {
      this.hideLoader();
      
    }, 1000);
  }

  hideLoader() {
    this.ionLoader.hideLoader();
  }

}
