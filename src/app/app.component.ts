import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Platform, MenuController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AndroidPermissions } from '@awesome-cordova-plugins/android-permissions/ngx';
import { App } from '@capacitor/app';





@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

  currentPageTitle = 'Dashboard';

  appPages = [
    {
      title: 'Dashboard',
      url: '',
      icon: 'easel'
    },
    {
      title: 'Timeline',
      url: '/timeline',
      icon: 'film'
    },
    {
      title: 'Settings',
      url: '/settings',
      icon: 'settings'
    }
  ];
  Fullname: any;
  hasPermission;
  constructor(

    private platform: Platform,
    private router: Router,
    private storage: Storage,
    public menuCtrl: MenuController,
    private http: HttpClient,
    private androidPermissions: AndroidPermissions
  ) {


    this.initializeApp();
  }



  initializeApp() {
    this.platform.ready().then(() => {
      this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE]).then(() => {
        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).then(result => {
          if (result.hasPermission == true) {
            this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(result => {
              if (result.hasPermission == true) {
                this.checkUser();
              }
              else {
                App.exitApp();
              }
            })
          }
          else {
            App.exitApp();
          }
        })
      })
    });
    this.platform.backButton.subscribeWithPriority(9999, () => {
      document.addEventListener('backbutton', function (event) {
        event.preventDefault();
        event.stopPropagation();
      }, false);
    });
  }

  Logout() {
    this.storage.remove('loginuid');
    this.menuCtrl.close();
    this.router.navigate(['/login']);
    // window.location.reload();

    // this.route.navigate(['/viewqrprofile/10']);
  }

  checkUser() {
    this.storage.get('loginuid').then((val) => {
      if (val) { // if not login
        this.router.navigateByUrl('/dashboard');
        /*
        this will also work
        this.navCtrl.goRoot('/');
       */
      } else { // if login
        this.router.navigateByUrl('/login');
        /* this will also work
         this.navCtrl.goRoot('/app');
        */
      }
    });
  }
  gohome() {
    this.menuCtrl.close();
    // window.location.reload();

    // this.route.navigate(['/viewqrprofile/10']);
  }


  dotasksall(loginuid) {
    var url = 'https://m-prolink.com/Edinfi/services/getuserdetails.php?ID=' + loginuid;
    console.log(url);
    this.http.get(url).subscribe(data => {
      // console.log(data['FirstName']);
      this.Fullname = data['Fullname'];

    }, err => { console.log(err); });



  }

}

