import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { LoaderService } from '../services/loader.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  public emailaddress: string;
  public password: string;
  public cpassword: string;
  public fullname:string;
  public fathername:string;
  public phonenumber:string;
  public studentid:string;


  constructor(public navCtrl: NavController, private http: HttpClient,private ionLoader: LoaderService,private router:Router){ }

  ngOnInit() {
  }

  
  register() {

    if(this.fullname=='')
    {
      
      jQuery("#ErrorSignin").text('Enter Fullname!');
      jQuery(".ErroDiv").slideDown();
      setTimeout(function() {
        $('.ErroDiv').slideUp("slow");
    }, 5500);
    }
    else if(this.fathername == '')
    {
      
      jQuery("#ErrorSignin").text('Enter Fathername!');
      jQuery(".ErroDiv").slideDown();
      setTimeout(function() {
        $('.ErroDiv').slideUp("slow");
    }, 5500);
    }
    else if(this.emailaddress == '')
    {
      
      jQuery("#ErrorSignin").text('Enter Emailaddress!');
      jQuery(".ErroDiv").slideDown();
      setTimeout(function() {
        $('.ErroDiv').slideUp("slow");
    }, 5500);
    }
    else if(this.phonenumber == '')
    {
      
      jQuery("#ErrorSignin").text('Enter Phonenumber!');
      jQuery(".ErroDiv").slideDown();
      setTimeout(function() {
        $('.ErroDiv').slideUp("slow");
    }, 5500);
    }
    else if(this.password == '')
    {
      
      jQuery("#ErrorSignin").text('Enter Password!');
      jQuery(".ErroDiv").slideDown();
      setTimeout(function() {
        $('.ErroDiv').slideUp("slow");
    }, 5500);
    }
    else if(this.password != this.cpassword)
    {
      
      jQuery("#ErrorSignin").text('Password are not same!');
      jQuery(".ErroDiv").slideDown();
      setTimeout(function() {
        $('.ErroDiv').slideUp("slow");
    }, 5500);
    }
    else
    {
    var url = 'https://arttcssacademy.com/app/services/getregister.php?Fullname='+this.fullname+'&Fathername='+this.fathername+'&Phonenumber='+this.phonenumber+'&EmailAddress='+this.emailaddress+'&Password='+this.password;
   console.log(url);
    this.http.get(url).subscribe(data=>{
     
      
      if(data[0]['status']=='alert')
      {
         
      jQuery("#ErrorSignin").text('Email already exist!');
      jQuery(".ErroDiv").slideDown();
      setTimeout(function() {
        $('.ErroDiv').slideUp("slow");
    }, 5500);
        //  console.log(data[0]['msg']);
      }
      else
      {
        
        this.router.navigateByUrl('/login');
        jQuery(".SuccessDiv").slideDown();
        this.fullname="";
        this.fathername="";
        this.password="";
        this.cpassword="";
        this.emailaddress="";
        setTimeout(function() {

      }, 5500);

      }
      
		  },err=>{console.log(err);});
    
    }
  }
  selecttype(phonenumberval)
  {
    this.phonenumber = phonenumberval;

  }

  showLoader() {
    this.ionLoader.showLoader();

    this.register();
    setTimeout(() => {
      this.hideLoader();
      
    }, 1000);
  }

  hideLoader() {
    this.ionLoader.hideLoader();
  }

}
