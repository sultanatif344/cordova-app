import { Injectable } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, NavigationEnd, NavigationExtras, Router } from '@angular/router';
import { NavController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {

  constructor(private router: Router, private acitvatedRoute: ActivatedRoute, private navCtrl: NavController) { }

  push(routeName, routeParams) {
    return new Promise(async resolve => {
      const extras: NavigationExtras = {
        queryParams: {
          routeParams
        }
      };
      console.log(extras);
      this.router.navigateByUrl(routeName, extras);
      resolve(true);
    }).catch((error)=>{
      console.log(error)
    });
  }

  getQueryParams(){
      const currenState = this.router.getCurrentNavigation();
      return currenState.extras.queryParams?.routeParams;
  }
  

  pop(){
    this.navCtrl.pop();
  };

  setRoot(routeName){
    this.navCtrl.navigateRoot(routeName);
  }
}
