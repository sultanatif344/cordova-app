import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(private alertController: AlertController) { }

  showBasicAlert(msg): Promise<boolean> {
    return new Promise(async resolve => {
      const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: '',
        subHeader: '',
        message: `<h4>${msg}</h4>`,
        mode:'ios',
        buttons: ['OK'],
      });
      alert.present();
      resolve(true);
    });
  }
}
