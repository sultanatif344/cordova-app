import { Injectable } from '@angular/core';
import { AlertService } from './alert.service';

@Injectable({
  providedIn: 'root'
})
export class UtilityService {

  constructor(private alertService: AlertService) { }


  async presentBasicAlert(msg){
    const alertRes = await this.alertService.showBasicAlert(msg); 
  }
}
