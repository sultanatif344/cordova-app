import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class NetworkService {

  public headers = new HttpHeaders();
  constructor(private http: HttpClient) { }

  public getAllQuiz(formData): Observable<any> {
    console.log(formData);
    this.headers.append('content-type','application/json');
      return this.http.post(`${environment.baseUrl}getallquiz.php`, formData,{
        headers: this.headers
      });
  }

  public getAllQuestionsFromQuizId(formData): Observable<any>{
    this.headers.append('content-type','application/json');
    return this.http.post(`${environment.baseUrl}getallquestions.php`, formData,{
      headers: this.headers
    });
  }
}
