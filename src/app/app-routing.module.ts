import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  // {
  //   path: '',
  //   redirectTo: 'dashboard',
  //   pathMatch: 'full'
  // },
  {
    path: 'dashboard',
    loadChildren: () => import('./dashboard/dashboard.module').then( m => m.DashboardPageModule)
  },
  // {
  //   path: 'home',
  //   loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  // },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'coursepage/:id',
    loadChildren: () => import('./coursepage/coursepage.module').then( m => m.CoursepagePageModule)
  },
  {
    path: 'coursetopics/:id',
    loadChildren: () => import('./coursetopics/coursetopics.module').then( m => m.CoursetopicsPageModule)
  },
  {
    path: 'courserunning/:id',
    loadChildren: () => import('./courserunning/courserunning.module').then( m => m.CourserunningPageModule)
  },
  {
    path: 'coursertopicsingle/:cid/:id/:ic',
    loadChildren: () => import('./coursertopicsingle/coursertopicsingle.module').then( m => m.CoursertopicsinglePageModule)
  },
  {
    path: 'faqs/:id',
    loadChildren: () => import('./faqs/faqs.module').then( m => m.FaqsPageModule)
  },
  {
    path: 'aboutus',
    loadChildren: () => import('./aboutus/aboutus.module').then( m => m.AboutusPageModule)
  },
  {
    path: 'contactus',
    loadChildren: () => import('./contactus/contactus.module').then( m => m.ContactusPageModule)
  },
  {
    path: 'moodalenroll/:id',
    loadChildren: () => import('./moodalenroll/moodalenroll.module').then( m => m.MoodalenrollPageModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('./profile/profile.module').then( m => m.ProfilePageModule)
  },
  {
    path: 'seeallcourse',
    loadChildren: () => import('./seeallcourse/seeallcourse.module').then( m => m.SeeallcoursePageModule)
  },
  {
    path: 'forgetpassword',
    loadChildren: () => import('./forgetpassword/forgetpassword.module').then( m => m.ForgetpasswordPageModule)
  },
  {
    path: 'entercode',
    loadChildren: () => import('./entercode/entercode.module').then( m => m.EntercodePageModule)
  },
  {
    path: 'seeallcoursenew',
    loadChildren: () => import('./seeallcoursenew/seeallcoursenew.module').then( m => m.SeeallcoursenewPageModule)
  },
  {
    path: 'cfaqs/:cid/:id/:ic',
    loadChildren: () => import('./cfaqs/cfaqs.module').then( m => m.CfaqsPageModule)
  },
  {
    path: 'quiz',
    loadChildren: () => import('./quiz/quiz.module').then( m => m.QuizPageModule)
  },


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules})
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
