import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NavController,ViewWillEnter } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-faqs',
  templateUrl: './faqs.page.html',
  styleUrls: ['./faqs.page.scss'],
})
export class FaqsPage implements ViewWillEnter {
  connectionslist: any;
  searchvalue="";
  zitappurl: string;
  totalcards: any;
  cid: string;
  constructor(private _Activatedroute:ActivatedRoute,public navCtrl: NavController, private http: HttpClient,private storage: Storage,private route: Router){ 
    
    this.cid =this._Activatedroute.snapshot.paramMap.get("id");
  }

  ionViewWillEnter() {

         
    var url = 'https://arttcssacademy.com/app/services/getallfaqs.php?CID='+this.cid;
    console.log(url);
     this.http.get(url).subscribe(data=>{
       console.log(data);
       this.connectionslist = data;
       this.totalcards = data['length'];
       
       console.log(this.connectionslist);
       },err=>{console.log(err);});
  }

  
  changetoggle(fid)
  {
   if($("#faq-"+fid).attr("value")=='0')
   {
    $("#faq-"+fid).slideDown();
    $("#faq-"+fid).attr("value","1");
   }
    else
    {
    $("#faq-"+fid).slideUp();
    $("#faq-"+fid).attr("value","0");
    }
  }

}
