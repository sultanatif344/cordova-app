import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NavController,ViewWillEnter } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-coursetopics',
  templateUrl: './coursetopics.page.html',
  styleUrls: ['./coursetopics.page.scss'],
})
export class CoursetopicsPage implements ViewWillEnter {
  connectionslist: any;
  courseid:any;
  totaltopics: any;
  ID: any;
  Logo: any;
  CourseName: any;
  Duration: any;
  ShortDescription: any;
  Description: any;

  constructor(private _Activatedroute:ActivatedRoute,public navCtrl: NavController, private http: HttpClient,private storage: Storage,private route: Router){ 

    this.courseid =this._Activatedroute.snapshot.paramMap.get("id");
  }

  ionViewWillEnter() {


    
    var url = 'https://arttcssacademy.com/app/services/getcoursedetails.php?ID='+this.courseid;
    console.log(url);
    this.http.get(url).subscribe(data=>{
      console.log(data);
      this.ID = data['ID'];
      this.Logo = data['Logo'];
      this.CourseName = data['CourseName'];
      this.Duration = data['Duration'];
      this.ShortDescription = data['ShortDescription'];
      this.Description = data['Description'];
     
      
		  },err=>{console.log(err);});


      
     
    var url = 'https://arttcssacademy.com/app/services/getalltopics.php?UID='+this.courseid;
    console.log(url);
     this.http.get(url).subscribe(data=>{
       console.log(data);
       this.connectionslist = data;
       this.totaltopics = data['length'];
       
       console.log(this.connectionslist);
       },err=>{console.log(err);});
  }

}
