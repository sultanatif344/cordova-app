import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CoursetopicsPageRoutingModule } from './coursetopics-routing.module';

import { CoursetopicsPage } from './coursetopics.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CoursetopicsPageRoutingModule
  ],
  declarations: [CoursetopicsPage]
})
export class CoursetopicsPageModule {}
