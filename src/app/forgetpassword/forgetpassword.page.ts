import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { LoaderService } from '../services/loader.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgetpassword',
  templateUrl: './forgetpassword.page.html',
  styleUrls: ['./forgetpassword.page.scss'],
})
export class ForgetpasswordPage implements OnInit {
  public emailaddress: string;
  public password: string;
  public cpassword: string;
  public fullname:string;
  public fathername:string;
  public phonenumber:string;


  constructor(public navCtrl: NavController, private http: HttpClient,private ionLoader: LoaderService,private router:Router){ }

  ngOnInit() {
  }

  
  forgetpassword() {

   if(this.emailaddress == '')
    {
      
      jQuery("#ErrorSignin").text('Enter Emailaddress!');
      jQuery(".ErroDiv").slideDown();
      setTimeout(function() {
        $('.ErroDiv').slideUp("slow");
    }, 5500);
    }
    else
    {
  //   var url = 'https://m-prolink.com/Edinfi/services/getregister.php?Fullname='+this.fullname+'&Fathername='+this.fathername+'&Phonenumber='+this.phonenumber+'&EmailAddress='+this.emailaddress+'&Password='+this.password;
  //  console.log(url);
  //   this.http.get(url).subscribe(data=>{
     
      
  //     if(data[0]['status']=='alert')
  //     {
         
  //     jQuery("#ErrorSignin").text('Email already exist!');
  //     jQuery(".ErroDiv").slideDown();
  //     setTimeout(function() {
  //       $('.ErroDiv').slideUp("slow");
  //   }, 5500);
  //     }
  //     else
  //     {
        
  //       this.router.navigateByUrl('/login');
  //       jQuery(".SuccessDiv").slideDown();
  //       this.fullname="";
  //       this.fathername="";
  //       this.password="";
  //       this.cpassword="";
  //       this.emailaddress="";
  //       setTimeout(function() {

  //     }, 5500);

  //     }
      
	// 	  },err=>{console.log(err);});
    
  this.router.navigate(['/entercode']);
    }
  }
 

  showLoader() {
    this.ionLoader.showLoader();

    this.forgetpassword();
    setTimeout(() => {
      this.hideLoader();
      
    }, 1000);
  }

  hideLoader() {
    this.ionLoader.hideLoader();
  }

}
