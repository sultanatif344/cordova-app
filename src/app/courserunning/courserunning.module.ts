import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CourserunningPageRoutingModule } from './courserunning-routing.module';

import { CourserunningPage } from './courserunning.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CourserunningPageRoutingModule
  ],
  declarations: [CourserunningPage]
})
export class CourserunningPageModule {}
