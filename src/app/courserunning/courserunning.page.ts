import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NavController,ViewWillEnter } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ActivatedRoute, Router } from '@angular/router';
import { NavigationService } from '../services/navigation.service';

@Component({
  selector: 'app-courserunning',
  templateUrl: './courserunning.page.html',
  styleUrls: ['./courserunning.page.scss'],
})
export class CourserunningPage implements ViewWillEnter {
  cid:string;
  public userdetails: any = [];
  Logo: any;
  CourseName: any;
  Duration: any;
  ShortDescription: any;
  Description: any;
  ID: any;
  TotalTopics: any;
  connectionslist: any;
  TotalCompleted: any;
  constructor(private _Activatedroute:ActivatedRoute,public navCtrl: NavController, private http: HttpClient,private storage: Storage,private route: Router, private navService: NavigationService){ 

    this.cid =this._Activatedroute.snapshot.paramMap.get("id");
  }

  ionViewWillEnter() {

    
    this.storage.get('loginuid').then((val) => {

    var url = 'https://arttcssacademy.com/app/services/getcoursedetails.php?ID='+this.cid+'&UID='+val;
    console.log(url);
    this.http.get(url).subscribe(data=>{
      console.log(data);
      this.ID = data['ID'];
      this.Logo = data['Logo'];
      this.CourseName = data['CourseName'];
      this.Duration = data['Duration'];
      this.ShortDescription = data['ShortDescription'];
      this.Description = data['Description'];
      this.TotalTopics = data['TotalTopics'];
      this.TotalCompleted = data['TotalCompleted'];
     
      
		  },err=>{console.log(err);});

      
 
          
    var url = 'https://arttcssacademy.com/app/services/getuserscompletedtopics.php?CID='+this.cid+'&UID='+val;
    console.log(url);
     this.http.get(url).subscribe(data=>{
       console.log(data);
       this.connectionslist = data;
       
       console.log(this.connectionslist);
       },err=>{console.log(err);});

      });


  }

  async goToQuiz(cid){
    console.log("this is the cid",cid);
    const res = await this.navService.push('quiz',cid);
  }

}
