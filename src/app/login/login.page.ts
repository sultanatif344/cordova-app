import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';

import { LoaderService } from '../services/loader.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  public emailaddress: string;
  public password: string;
  isregister: string;
  isLoggedIn = false;
  usersLogin = { 
      id: '', 
      name: '', 
      email: '', 
      picture: { 
          data: { 
              url: '' 
          } 
      } 
  };

  constructor(private _Activatedroute:ActivatedRoute,public navCtrl: NavController, private http: HttpClient,private storage: Storage,private ionLoader: LoaderService){ 
    this.isregister =this._Activatedroute.snapshot.paramMap.get("id");



  }

  


  
  ngOnInit() {

    document.addEventListener("backbutton",function(e) {
      console.log("disable back button")
    }, false);

    if(this.isregister == '1')
    {
      $('#texttochange').html('<h3 id="texttochange" style="color: #14ac1c;font-size: 16px;">Congratulations! You have created account successfully.</h3>');
    }
    
  }

  
  login() {
    // alert('saad');
    console.log(this.emailaddress);
    console.log(this.password);
    if(this.emailaddress=='')
    {
      
      jQuery("#ErrorSignin").text('Enter Password!');
      jQuery(".ErroDiv").slideDown();
      setTimeout(function() {
        $('.ErroDiv').slideUp("slow");
    }, 5500);
    }
    else if(this.password == '')
    {
      
      jQuery("#ErrorSignin").text('Enter Password!');
      jQuery(".ErroDiv").slideDown();
      setTimeout(function() {
        $('.ErroDiv').slideUp("slow");
    }, 5500);
    }
    else
    {

    var url = 'https://arttcssacademy.com/app/services/getlogin.php?EmailAddress='+this.emailaddress+'&Password='+this.password;
    console.log(url);
    this.http.get(url).subscribe(data=>{
     
       console.log(data[0]);
      if(data[0]['status']=='nsuccess')
      {
        
      jQuery("#ErrorSignin").text('Invalid Email or Password!');
        jQuery(".ErroDiv").slideDown();
        
         console.log(data[0]['msg']);
         setTimeout(function() {
          $('.ErroDiv').slideUp("slow");
      }, 5500);
      }
      else
      {
        console.log(data[0]['msg']);
        this.storage.set('loginuid', data[0]['userId']);
        this.navCtrl.navigateForward('/dashboard');
      }
		  },err=>{console.log(err);});
    
    }
  }


  showLoader() {
    this.ionLoader.showLoader();

    this.login();
    setTimeout(() => {
      this.hideLoader();
      
    }, 1000);
  }

  hideLoader() {
    this.ionLoader.hideLoader();
  }


}
