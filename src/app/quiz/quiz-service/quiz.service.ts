import { Injectable } from '@angular/core';
import { NetworkService } from 'src/app/services/network.service';

@Injectable({
  providedIn: 'root'
})
export class QuizService {

  constructor(private network: NetworkService) { }


  public getQuizesEntity(payload): Promise<any[]> {
    return new Promise(resolve => {
      console.log("this is the payload", payload)
      this.network.getAllQuiz(payload).subscribe(result => {
        resolve(result);
      }, (error) => {
        resolve([]);
      });
    });
  }

  public getQuestionsEntity(payload): Promise<any[]> {
    return new Promise(resolve => {
      this.network.getAllQuestionsFromQuizId(payload).subscribe(result => {
        resolve(result);
      }, (error) => {
        resolve([]);
      });
    });
  }
}
