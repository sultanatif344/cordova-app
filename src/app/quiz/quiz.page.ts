import { Component, OnInit } from '@angular/core';
import { ViewWillEnter } from '@ionic/angular';
import { NavigationService } from '../services/navigation.service';
import { NetworkService } from '../services/network.service';
import { QuizService } from './quiz-service/quiz.service';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.page.html',
  styleUrls: ['./quiz.page.scss'],
})
export class QuizPage implements OnInit, ViewWillEnter {
  quizesEntity = [];
  title = '';
  courseId = null;
  constructor(private navService: NavigationService, private quizService: QuizService) {
    this.initialize();

  }

  ionViewWillEnter(): void {
    console.log("hello")

  }

  ngOnInit(): void {
  }
  async initialize() {

    const params: any = this.navService.getQueryParams();
    const payload = new FormData();
    this.courseId = params;
    payload.append('course_id', this.courseId);
    if (this.courseId) {
      console.log("this is the course id", params);
      this.quizesEntity = await this.quizService.getQuizesEntity(payload);
      this.title = this.quizesEntity[0]?.coursename;
      console.log(this.quizesEntity);
    }
  }

  back() {
    this.navService.pop();
  }

  startQuiz(quizId) {
    this.navService.push('quiz/taken-quiz', JSON.stringify({ courseId: this.courseId, id:quizId }));
  }
}
