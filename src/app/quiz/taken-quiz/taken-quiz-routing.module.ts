import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TakenQuizPage } from './taken-quiz.page';

const routes: Routes = [
  {
    path: '',
    component: TakenQuizPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TakenQuizPageRoutingModule {}
