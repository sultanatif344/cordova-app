import { Component, OnInit, ViewChild } from '@angular/core';
import { NavigationService } from 'src/app/services/navigation.service';
import { QuizService } from '../quiz-service/quiz.service';
import { IonSlides, ViewDidEnter, ViewWillEnter } from '@ionic/angular';
import { UtilityService } from 'src/app/services/utility.service';
@Component({
  selector: 'app-taken-quiz',
  templateUrl: './taken-quiz.page.html',
  styleUrls: ['./taken-quiz.page.scss'],
})
export class TakenQuizPage implements OnInit, ViewDidEnter, ViewWillEnter {
  @ViewChild('mySlider', { static: true }) slides: IonSlides;
  allQuestions = [];
  selectedAnswer = "";
  correctAnswer = "";
  totalPoints = 0;
  currentActiveSlide;
  disabledImage = "../../assets/MCQicon.png";
  enabledImage = "../../assets/MCQicon1.png";
  params: any;
  constructor(private navService: NavigationService, private quizService: QuizService, private utility: UtilityService) {
    this.params = this.navService.getQueryParams();
  }


  ngOnInit() {
  }

  ionViewDidEnter() {
    this.slides.lockSwipes(true);
  }

  ionViewWillEnter() {
    this.resetQuiz();
    this.initialize();
  }
  async initialize() {

    console.log("quiz params", this.params);
    if (this.params) {
      const payload = new FormData();
      payload.append('course_id', JSON.parse(this.params).courseId);
      payload.append('quiz_id', JSON.parse(this.params).id);
      this.allQuestions = await this.quizService.getQuestionsEntity(payload);
      console.log("questions", this.allQuestions);
    }
  }
  back() {
    this.navService.pop();
  }

  async getSelectedValue(value, correctAnswer) {
    this.currentActiveSlide = await this.slides.getActiveIndex();
    this.selectedAnswer = value;
    this.correctAnswer = correctAnswer;
  }
  goToNextQuestion() {
    this.slides.lockSwipes(false);
    if (this.selectedAnswer != "") {
      if (this.selectedAnswer === this.correctAnswer) {
        this.totalPoints++;
        if (this.currentActiveSlide + 1 === this.allQuestions.length) {
          this.navService.push('quiz/results', this.totalPoints);
        }
        else {
          this.slides.slideNext(300);
          this.resetOptions();
        }
      }
      else {
        if (this.currentActiveSlide + 1 === this.allQuestions.length) {
          this.navService.push('quiz/results', this.totalPoints);
        }
        else {
          console.log(this.selectedAnswer);
          console.log(this.correctAnswer);
          console.log("this is running")
          this.slides.slideNext(300);
          this.resetOptions();
        }
      }
    }
    else {
      this.utility.presentBasicAlert("Kindly Select An Option");
    }

    this.slides.lockSwipes(true);
  }
  resetQuiz() {
    this.selectedAnswer = "";
    this.correctAnswer = "";
    this.currentActiveSlide = 0;
    this.totalPoints = 0;
    this.slides.lockSwipes(false);
    this.slides.slideTo(0, 800);
    this.slides.lockSwipes(true);
  }

  resetOptions() {
    this.selectedAnswer = "";
    this.correctAnswer = "";
  }
}
