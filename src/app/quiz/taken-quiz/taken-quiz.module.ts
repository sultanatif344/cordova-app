import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TakenQuizPageRoutingModule } from './taken-quiz-routing.module';

import { TakenQuizPage } from './taken-quiz.page';
import { QuizService } from '../quiz-service/quiz.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TakenQuizPageRoutingModule
  ],
  declarations: [TakenQuizPage],
  providers:[QuizService]
})
export class TakenQuizPageModule {}
