import { Component, OnInit } from '@angular/core';
import { NavigationService } from 'src/app/services/navigation.service';

@Component({
  selector: 'app-results',
  templateUrl: './results.page.html',
  styleUrls: ['./results.page.scss'],
})
export class ResultsPage implements OnInit {
  totalPoints = "";
  constructor(private nav: NavigationService) {
    this.initialize();
  }

  ngOnInit() {
  }

  async initialize() {
    this.totalPoints = this.nav.getQueryParams();
  }

  back() {
    this.nav.pop();
  }

  goToHome() {
    this.nav.setRoot('dashboard');
  }

}
