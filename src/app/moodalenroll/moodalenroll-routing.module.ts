import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MoodalenrollPage } from './moodalenroll.page';

const routes: Routes = [
  {
    path: '',
    component: MoodalenrollPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MoodalenrollPageRoutingModule {}
