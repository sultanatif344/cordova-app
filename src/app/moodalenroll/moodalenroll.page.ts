import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NavController,ViewWillEnter } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-moodalenroll',
  templateUrl: './moodalenroll.page.html',
  styleUrls: ['./moodalenroll.page.scss'],
})
export class MoodalenrollPage implements ViewWillEnter {
  cid:string;
  public userdetails: any = [];
  Logo: any;
  CourseName: any;
  Duration: any;
  ShortDescription: any;
  Description: any;
  ID: any;
  TotalTopics: any;
  constructor(private _Activatedroute:ActivatedRoute,public navCtrl: NavController, private http: HttpClient,private storage: Storage,private route: Router){ 

    this.cid =this._Activatedroute.snapshot.paramMap.get("id");
  }

  ionViewWillEnter() {

    var url = 'https://arttcssacademy.com/app/services/getcoursedetails.php?ID='+this.cid;
    console.log(url);
    this.http.get(url).subscribe(data=>{
      console.log(data);
      this.ID = data['ID'];
      this.Logo = data['Logo'];
      this.CourseName = data['CourseName'];
      this.Duration = data['Duration'];
      this.ShortDescription = data['ShortDescription'];
      this.Description = data['Description'];
      this.TotalTopics = data['TotalTopics'];
     
      
		  },err=>{console.log(err);});

      



  }

  makeenrolled()
  {
    
    this.storage.get('loginuid').then((val) => {
    var url = 'https://arttcssacademy.com/app/services/getenrolled.php?CID='+this.cid+'&UID='+val;
    console.log(url);
    this.http.get(url).subscribe(data=>{
      console.log(data);
      
      this.route.navigateByUrl('/courserunning/'+this.cid);
     
      
		  },err=>{console.log(err);});

    });
  }

}
