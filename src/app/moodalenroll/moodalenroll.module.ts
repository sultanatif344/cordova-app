import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MoodalenrollPageRoutingModule } from './moodalenroll-routing.module';

import { MoodalenrollPage } from './moodalenroll.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MoodalenrollPageRoutingModule
  ],
  declarations: [MoodalenrollPage]
})
export class MoodalenrollPageModule {}
