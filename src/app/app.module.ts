import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import * as $ from 'jquery';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { SplashScreen } from '@awesome-cordova-plugins/splash-screen/ngx';
import { ProgressBarComponent } from './progress-bar/progress-bar.component';
import { AndroidPermissions } from '@awesome-cordova-plugins/android-permissions/ngx';





@NgModule({
  declarations: [AppComponent,ProgressBarComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), IonicStorageModule.forRoot(),AppRoutingModule,HttpClientModule],
  providers: [HttpClient,{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy },SplashScreen,AndroidPermissions],
  bootstrap: [AppComponent],
})
export class AppModule {}
