import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CoursepagePage } from './coursepage.page';

const routes: Routes = [
  {
    path: '',
    component: CoursepagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CoursepagePageRoutingModule {}
