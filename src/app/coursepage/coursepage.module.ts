import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CoursepagePageRoutingModule } from './coursepage-routing.module';

import { CoursepagePage } from './coursepage.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CoursepagePageRoutingModule
  ],
  declarations: [CoursepagePage]
})
export class CoursepagePageModule {}
